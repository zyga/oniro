# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

require recipes-core/images/core-image-base.bb
require oniro-image-common.inc

SUMMARY = "Oniro Project image including the base OS software stack"
